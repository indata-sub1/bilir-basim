using HareketApi.Application;
using HareketApi.Models;
using ServiceStack.Text;
using SetCrmHelper;

var builder = WebApplication.CreateBuilder(args);

var appConfig = new AppConfig();
builder.Configuration.Bind(nameof(AppConfig), appConfig);
var corsPolicyName = appConfig.CorsOriginPolicyName;

//services.AddCors(options => {
//    options.AddDefaultPolicy(

//        builder => builder.WithOrigins("https://localhost:44325"));
//    options.AddPolicy("myPolicy", builder => builder.WithOrigins("https://demoevim.indata.com.tr"));
//});

builder.Services.AddCors(c =>
{
    c.AddPolicy(corsPolicyName, options => options
        .WithOrigins(appConfig.CorsOrigins.Split(",", StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToArray())
        .AllowAnyHeader()
        .AllowAnyMethod()
        .AllowCredentials());
});


builder.Services.AddTransient<ISetCrmHelper,SetCrmHelperr>();
builder.Services.AddTransient<IHareketService, SetCrmHareketService>();
// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


var app = builder.Build();

app.UseCors(corsPolicyName);

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

//app.UseCors(options => options.AllowAnyOrigin());

