﻿using HareketApi.Models;
using SetCrmHelper;
using SetCrmHelper.MayaModels;

namespace HareketApi.Application
{
    public class SetCrmHareketService : IHareketService
    {
        private readonly ISetCrmHelper _setCrmHelper;
        public SetCrmHareketService(ISetCrmHelper setCrmHelper)
        {
            _setCrmHelper = setCrmHelper;
        }

        /*
            {
                "CustomObjectId" : "57FA4F51296740368BABB61E1B16AB3F",
                "FieldsValues" :{
                    "F3B2C3D4E9954BC4A30AA05EA474EC36": "2022.03.14T05:00:00",
                    "D6CCE06981234A5CA4D83E78FECF18CF": "AC6CAE0D2D9B4993AC5C418370898CFF",
                    "7DC019A709A349A682D805B61D52136C": "902CB693AEFA411B9238DE5724291227"
                }
            }
         */
        public async Task StartAsync(HareketInputModel input)
        {
            var apiRequestModel = new RecordRequestParameters()
            {
                CustomObjectId = "4F8DC1FAF7EE45EABA02B632A451F513",
                IsForcingSave = false,
                FieldsValues = new Dictionary<string, object>()
            };
           // apiRequestModel.FieldsValues.Add("3B9D8D47794D44D89B140DD1BC7A9CF3", DateTime.Now);//.ToString("yyyy.MM.ddTHH:mm:ss")
            apiRequestModel.FieldsValues.Add("ED0240AB09B842618B21B2FB14A8890E", input.HareketUserId);
            apiRequestModel.FieldsValues.Add("E8E508571E0348EC95D9394CB1940264", "33D7F42B70D3491288ABB9C461C3580A");
            apiRequestModel.FieldsValues.Add("7423EC9F0E824754B10F4A547ECFB59B", "0D6533B8A34B44918C698C7C9E69D1CF");
            apiRequestModel.FieldsValues.Add("3B9D8D47794D44D89B140DD1BC7A9CF3",  DateTime.Now);

            await _setCrmHelper.PostRecordAsync(apiRequestModel);
        }

        public async Task PauseAsync(HareketInputModel input)
        {
            var apiRequestModel = new RecordRequestParameters()
            {
                CustomObjectId = "4F8DC1FAF7EE45EABA02B632A451F513",
                IsForcingSave = false,
                FieldsValues = new Dictionary<string, object>()
            };
            apiRequestModel.FieldsValues.Add("3B9D8D47794D44D89B140DD1BC7A9CF3", DateTime.Now);
            apiRequestModel.FieldsValues.Add("ED0240AB09B842618B21B2FB14A8890E", input.HareketUserId);
            apiRequestModel.FieldsValues.Add("E8E508571E0348EC95D9394CB1940264", "2CDBD74E9FEE4896898F0FC3701FFE00");

            await _setCrmHelper.PostRecordAsync(apiRequestModel);
        }


        public async Task StopAsync(HareketInputModel input)
        {
            var apiRequestModel = new RecordRequestParameters()
            {
                CustomObjectId = "4F8DC1FAF7EE45EABA02B632A451F513",
                IsForcingSave = false,
                FieldsValues = new Dictionary<string, object>()
            };
            apiRequestModel.FieldsValues.Add("3B9D8D47794D44D89B140DD1BC7A9CF3", DateTime.Now);
            apiRequestModel.FieldsValues.Add("ED0240AB09B842618B21B2FB14A8890E", input.HareketUserId);
            apiRequestModel.FieldsValues.Add("E8E508571E0348EC95D9394CB1940264", "D640B622E7434C1CB233E969A28A029C");
            apiRequestModel.FieldsValues.Add("7423EC9F0E824754B10F4A547ECFB59B", "0D6533B8A34B44918C698C7C9E69D1CF");

            await _setCrmHelper.PostRecordAsync(apiRequestModel);
        }
        
        public async Task PutAsync(HareketInputModel input)
        {
            var apiRequestModel = new RecordRequestParameters()
            {
                CustomObjectId = "4F8DC1FAF7EE45EABA02B632A451F513",
                IsForcingSave = false,
                FieldsValues = new Dictionary<string, object>()
            };
            apiRequestModel.FieldsValues.Add("3B9D8D47794D44D89B140DD1BC7A9CF3", DateTime.Now);
            apiRequestModel.FieldsValues.Add("ED0240AB09B842618B21B2FB14A8890E", input.HareketUserId);
            apiRequestModel.FieldsValues.Add("E8E508571E0348EC95D9394CB1940264", "2CDBD74E9FEE4896898F0FC3701FFE00");

            await _setCrmHelper.PostRecordAsync(apiRequestModel);
        }
    }
}
