﻿namespace HareketApi.Models
{
    public class AppConfig
    {
        public string CorsOrigins { get; set; }
        public string CorsOriginPolicyName { get; set; }
    }
}
